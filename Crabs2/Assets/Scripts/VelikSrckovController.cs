﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class VelikSrckovController : MonoBehaviour {
    public List<SrcekController> ListOfSrcekControllers;

    public List<Material> Materials;

    public void DestroyThem(float delayTime) {
        foreach (var listOfSrcekController in ListOfSrcekControllers) {
            StartCoroutine(listOfSrcekController.Destroy(delayTime));
        }
    }

    private void Start() {
        SetRandomMaterialToAll();
    }

    public void SetRandomMaterialToAll() {
        foreach (var listOfSrcekController in ListOfSrcekControllers) {
            int index = Random.Range(0, Materials.Count);
            listOfSrcekController.MeshRenderer.material = Materials[index];
        }
    }
}