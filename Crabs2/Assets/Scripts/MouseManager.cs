﻿using UnityEngine;

public static class MouseManager {
    public static Transform MousePointerPosition;
    public static Camera Camera;
    public static CursorUIController CursorUIController;

    public static Vector2 GetPixelCoordinatesOfMouse() {
        return Camera.WorldToScreenPoint(MousePointerPosition.position);
    }
}