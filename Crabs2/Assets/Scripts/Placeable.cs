﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Placeable : MonoBehaviour {
    public List<GameObject> SnapPoints;
    public MeshCollider Collider;
    public MeshRenderer MeshRenderer;
    public TagToSnapTo TagToSnapTo;
    public Material OpaqueMat;
    public Material TransparentMat;
    public Material TransparentUnplaecableMat;
    public CollisionCache CollisionCache;

    public bool GhostModeEnabled;

    public FlatSurfacePoints FlatSurfacePoints;
    public LayerMask FlatSurfaceLayerMask;
    public float maxSurfaceDifference;
    public bool CanPlace = false;
    public bool CanRotate = true;
    public bool CanBePickedUp = true;


    public List<GameObject> SmokePositions = new List<GameObject>();
    public AudioClip BuildSound;
    public GameObject DustEffect;
    public AudioSource AudioSource;

    private List<Vector3> SmokeVFXPositions = new List<Vector3>();

    [NonSerialized] public GameObject SnapedOnPoint;

    [NonSerialized] public Placeable Prefab;


    public void SpawnIt(Placeable placeable) {
        Prefab = placeable;
        GhostModeEnabled = false;
        foreach (var snapPoint in SnapPoints) {
            snapPoint.SetActive(true);
        }

        Collider.enabled = true;
        MeshRenderer.material = OpaqueMat;
        Collider.isTrigger = false;
        Collider.convex = false;

//        if (TagToSnapTo == TagToSnapTo.FreeForm) {
        Rigidbody rig = gameObject.GetComponent<Rigidbody>();
        if (rig == null) {
            Rigidbody rigidbody = gameObject.AddComponent<Rigidbody>();
            rigidbody.isKinematic = true;
        }
        else {
            rig.isKinematic = true;
        }

//        }

        foreach (var smokePosition in SmokePositions) {
            SmokeVFXPositions.Add(smokePosition.transform.position);
        }

        foreach (var smokeVfxPosition in SmokeVFXPositions) {
            GameObject spawnedVFX =
                Instantiate(DustEffect, smokeVfxPosition, Quaternion.identity);
            Destroy(spawnedVFX, 5f);
        }

        SubtitlesManager.SetSubtitleText("[buildy]");
        AudioSource.pitch = UnityEngine.Random.Range(0.8f, 1.3f);
        AudioSource.volume = 0.5f;
        AudioSource.PlayOneShot(BuildSound);
    }

    public void GhostMode() {
        CollisionCache.IgnoreBaseBuildingCollisions = TagToSnapTo != TagToSnapTo.FreeForm;
        Collider.gameObject.layer = LayerMask.NameToLayer("GhostMesh");

        GhostModeEnabled = true;
        foreach (var snapPoint in SnapPoints) {
            // dont remove, interfirse with placing
            snapPoint.SetActive(false);
        }

        Collider.enabled = true;
        Collider.convex = true;
        Collider.isTrigger = true;

        MeshRenderer.material = TransparentMat;
    }

    private void Update() {
        if (GhostModeEnabled) {
            if (CollisionCache.IsTocuhingSomething || (TagToSnapTo == TagToSnapTo.FreeForm && !IsOnFlatSurface())) {
                CanPlace = false;
                MeshRenderer.material = TransparentUnplaecableMat;
            }
            else {
                CanPlace = true;
                MeshRenderer.material = TransparentMat;
            }
        }
    }

    public bool IsOnFlatSurface() {
        float max = float.MinValue;
        float min = float.MaxValue;
        RaycastHit hitInfo;
        foreach (Transform point in FlatSurfacePoints.Points) {
            if (Physics.Raycast(point.position, Vector3.down, out hitInfo, Mathf.Infinity, FlatSurfaceLayerMask)) {
                if (hitInfo.point.y > max) {
                    max = hitInfo.point.y;
                }
                else if (hitInfo.point.y < min) {
                    min = hitInfo.point.y;
                }
            }
        }

        return (max - min) < maxSurfaceDifference;
    }
}