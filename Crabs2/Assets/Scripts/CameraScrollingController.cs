﻿using UnityEngine;

public class CameraScrollingController : MonoBehaviour {
    public float Speed = 0.2f;
    public Transform MainCameraTransform;

    public Transform CameraMovementTransform;

    public float ScrollDownLimit = 3.0f;
    public float ScrollUpLimit = 30.0f;

    private Vector3 TargetPosition;
    private Vector3 velocity = Vector3.zero;

    private void Start() {
        Vector3 temp = CameraMovementTransform.rotation.eulerAngles;
        CameraMovementTransform.rotation = Quaternion.Euler(temp);
        TargetPosition = MainCameraTransform.position;
    }

    private void Update() {
        float scrollSpeed = -Input.GetAxisRaw("Mouse ScrollWheel") * Speed * Time.deltaTime;

        TargetPosition = new Vector3(MainCameraTransform.position.x,
            Mathf.Clamp(TargetPosition.y + scrollSpeed, ScrollDownLimit, ScrollUpLimit),
            MainCameraTransform.position.z);


//        MainCameraTransform.position = new Vector3(MainCameraTransform.position.x,
//            scrollSpeed, MainCameraTransform.position.z);

        MainCameraTransform.position =
            Vector3.SmoothDamp(MainCameraTransform.position, TargetPosition, ref velocity, 0.3F);
    }
}