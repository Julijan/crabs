﻿using UnityEngine;

public class ClickableGameObject : MonoBehaviour {
    public bool Interectable = true;

    public virtual void MouseClickedDown() {
    }

    public virtual void MouseClickedUp() {
    }

    public virtual void MouseClickedFocusLost() {
    }

    public virtual void MouseClickedFocusEnter() {
    }
}