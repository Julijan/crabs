﻿using UnityEngine.Events;

public class ButtonController : ClickableGameObject {
    public UnityEvent ClickFunction;

    public override void MouseClickedDown() {
        if (ClickableGameObjectsManager.DoesManagerAllowClicks) {
            ClickFunction.Invoke();
        }
    }
}