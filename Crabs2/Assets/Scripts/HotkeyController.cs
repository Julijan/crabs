﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class HotkeyController : MonoBehaviour {
    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (!SceneManager.GetSceneByName("PauseMenu").isLoaded) {
                MouseManager.CursorUIController.IsEnabled = false;
                Cursor.visible = true;
                SceneManager.LoadScene("PauseMenu", LoadSceneMode.Additive);
                Time.timeScale = 0;
            }
            else {
                Cursor.visible = false;
                MouseManager.CursorUIController.IsEnabled = true;
                SceneManager.UnloadSceneAsync("PauseMenu");
                Time.timeScale = 1;
            }
        }
    }
}