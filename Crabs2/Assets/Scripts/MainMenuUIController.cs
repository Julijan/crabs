﻿using System;
using System.Collections;
using System.Collections.Generic;
using QFSW.MOP2;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuUIController : MonoBehaviour {
    public MainMenuButtonController CrabsButtonController;
    public MainMenuButtonController PlayButtonController;
    public MainMenuButtonController ExitButtonController;

    public VelikSrckovController VelikSrckov;
    public Transform SpawnPosition;

    public List<VelikSrckovController> PoolOfSrckov = new List<VelikSrckovController>();

    const float fpsMeasurePeriod = 0.5f;
    private int m_FpsAccumulator = 0;
    private float m_FpsNextPeriod = 0;
    private int m_CurrentFps = 60;

    private bool FPSChanged = false;

    public GameObject Camera;
    public AudioSource AudioSource;
    public AudioClip AudioClip;
    public AudioClip ChonkiesClip;

    public GameObject CameraPlaySelectionLocation;
    public GameObject CameraNormalSelectionLocation;
    public GameObject CameraChallangesSelectionLocation;
    private GameObject CurrentCameraLocation;
    public float CameraPanSpeed;
    public float CameraPanSpeedLimit;


    private void Start() {
        CurrentCameraLocation = CameraNormalSelectionLocation;
        m_FpsNextPeriod = Time.realtimeSinceStartup + fpsMeasurePeriod;
        AudioSource.PlayOneShot(ChonkiesClip);
    }

    private void Update() {
        // measure average frames per second
        m_FpsAccumulator++;
        if (Time.realtimeSinceStartup > m_FpsNextPeriod) {
            m_CurrentFps = (int) (m_FpsAccumulator / fpsMeasurePeriod);
            m_FpsAccumulator = 0;
            m_FpsNextPeriod += fpsMeasurePeriod;
        }

        if (Time.timeSinceLevelLoad > 2) {
            if (m_CurrentFps < 50.0f && !FPSChanged) {
                FPSChanged = true;
                CrabsButtonController.Interectable = false;
                CrabsButtonController.SetSpriteToInterectable(false);
            }
            else if (m_CurrentFps > 50.0f && FPSChanged) {
                CrabsButtonController.Interectable = true;
                CrabsButtonController.SetSpriteToInterectable(true);
                FPSChanged = false;
            }
        }

        Vector3 dir = CameraPanSpeed * Time.deltaTime *
                      (CurrentCameraLocation.transform.position - Camera.transform.position);
        dir = Vector3.ClampMagnitude(dir, CameraPanSpeedLimit);
        Camera.transform.position += dir;
    }


    public void CrabsClick() {
        if (PoolOfSrckov.Count >= 10) {
            int indexToDestroy = PoolOfSrckov.Count - 10;
            float destroyDelayTime = MyMathUtils.Linear(PoolOfSrckov.Count, 10, 16, 3, 0);
            if (destroyDelayTime < 0) destroyDelayTime = 0f;
            PoolOfSrckov[indexToDestroy].DestroyThem(destroyDelayTime);
            StartCoroutine(RemoveFromList(PoolOfSrckov[indexToDestroy]));
        }

        VelikSrckovController velikSrckovController =
            Instantiate(VelikSrckov, SpawnPosition.position, Quaternion.identity);
        PoolOfSrckov.Add(velikSrckovController);
    }

    public IEnumerator RemoveFromList(VelikSrckovController toRemove) {
        yield return new WaitForSeconds(5f); // 5 sec je 3 sec + 2 za die animacijo
        PoolOfSrckov.Remove(toRemove);
        Destroy(toRemove.gameObject);
    }

    public void PlayClick() {
        CurrentCameraLocation = CameraPlaySelectionLocation;
    }

    public void HomeClick() {
        SceneLoaderManager.ScreenToLoad = "SandBox";
        SceneLoaderManager.ScreenToUnLoad = "MainMenuScene";
        SceneManager.LoadScene("LoadingScene", LoadSceneMode.Additive);
    }

    public void BackClick() {
        CurrentCameraLocation = CameraNormalSelectionLocation;
    }

    public void ChallangesClick() {
        CurrentCameraLocation = CameraChallangesSelectionLocation;
    }

    public void ExitClick() {
        Rigidbody rig = Camera.AddComponent<Rigidbody>();
        rig.AddTorque(Camera.transform.TransformDirection(transform.forward * 100));
        Camera.AddComponent<SphereCollider>();
        Camera.AddComponent<OnCollisionExitGame>();
        AudioSource.PlayOneShot(AudioClip);
    }
}