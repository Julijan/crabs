﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour {
    public void Resume() {
        Cursor.visible = false;
        MouseManager.CursorUIController.IsEnabled = true;
        SceneManager.UnloadSceneAsync("PauseMenu");
        Time.timeScale = 1;
    }

    public void Settings() {
    }

    public void Exit() {
        SceneManager.UnloadSceneAsync("PauseMenu");
        SceneLoaderManager.ScreenToLoad = "MainMenuScene";
        SceneLoaderManager.ScreenToUnLoad = "SandBox";
        SceneManager.LoadScene("LoadingScene", LoadSceneMode.Additive);
        Time.timeScale = 1;
    }
}