﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MainMenuButtonController : ClickableGameObject {
    public Animator Animator;
    public LayerMask ExplosionLayerMask;
    public float ExplosionForce;
    public AudioSource AudioSource;
    public AudioClip AudioClipClickDown;
    public AudioClip AudioClipClickUp;
    public UnityEvent ClickFunction;
    public Material UninteractableMaterial;
    public Material InteractableMaterial;
    public MeshRenderer MeshRenderer;

    private bool ButtonClicked = false;
    private bool fromUninteractable = false;

    public void PlayInAnimation() {
        if (!Animator.GetCurrentAnimatorStateInfo(0).IsName("ButtonIn")) {
            Animator.SetTrigger("ButtonIn");
        }
    }

    public void PlayOutAnimation() {
        Animator.SetTrigger("ButtonOut");
        Explosion(transform.position, 3f);
    }

    void Explosion(Vector3 center, float radius) {
        Collider[] hitColliders = Physics.OverlapSphere(center, radius, ExplosionLayerMask);
        foreach (var hitCollider in hitColliders) {
            hitCollider.GetComponent<Rigidbody>().AddExplosionForce(ExplosionForce, center, 10f);
        }
    }

    public override void MouseClickedDown() {
        ButtonClicked = true;
        AudioSource.PlayOneShot(AudioClipClickDown);
        PlayInAnimation();
        SubtitlesManager.SetSubtitleText("[clicky]");
    }

    public override void MouseClickedUp() {
        if (ButtonClicked) {
            if (!fromUninteractable) {
                ClickFunction.Invoke();
            }

            fromUninteractable = false;

            ButtonClicked = false;
            AudioSource.PlayOneShot(AudioClipClickUp);
            PlayOutAnimation();
        }
    }

    public override void MouseClickedFocusLost() {
        if (ButtonClicked) {
            ButtonClicked = false;
            AudioSource.PlayOneShot(AudioClipClickUp);
            PlayOutAnimation();
        }
    }

    public void SetSpriteToInterectable(bool interectable) {
        if (interectable) {
            fromUninteractable = true;
            MeshRenderer.material = InteractableMaterial;
            MouseClickedUp();
            Animator.SetBool("Interectable", true);
        }
        else {
            ButtonClicked = true;
            MeshRenderer.material = UninteractableMaterial;
            AudioSource.PlayOneShot(AudioClipClickDown);
            Animator.SetBool("Interectable", false);
            PlayInAnimation();
        }
    }


//    void OnDrawGizmos() {
//        Gizmos.color = Color.yellow;
//        Gizmos.DrawSphere(transform.position, 3f);
//    }
}