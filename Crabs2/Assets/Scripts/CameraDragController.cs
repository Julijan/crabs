﻿using System;
using System.Linq;
using UnityEngine;

public class CameraDragController : MonoBehaviour {
    public Vector3 panOrigin;
    public float finalMoveStrenght;
    public float finalMoveSpeed;

    private bool bDragging;
    public bool DraggingDisabled;

    private bool FinalMoving;
    private Vector3 FinalPoint;

    private Buffer<Vector3> LastPositions = new Buffer<Vector3>(10);

    public float PositionRefreshRate = 0.015f;
    private float CurrentPosRefreshTime;

    public Camera Camera;
    public Transform CameraTransform;
    public float groundZ;

    public LayerMask FloorLayerMask;
    public LayerMask IgnoreSnapPointsMask;

    public float MaxPanSpeed = 1.0f;
    public float PanSpeed = 1.0f;
    public float PanSpeedBreak = 1.0f;

    public GameObject DustEffect;
    public GameObject WaterSplashEffect;
    public Vector3 DustEffectSpawnOffset;

    public AudioClip ClickDustSound;
    public AudioClip ClickWaterSound;
    public AudioSource WindAudioSource;

    public GameObject WindVFX;
    public ParticleSystem WindVFXParticleSystem;

    public MouseInputController MouseInputController;
    public CursorUIController CursorUIController;

    void LateUpdate() {
        // should be late, so that ClickedUIThisFrame works!
        if (FinalMoving) {
            MoveToFinalPos();
        }

        if (CursorUIController.ClickedUIThisFrame) return;

        if (Input.GetMouseButtonDown(0)) {
            Ray ray = Camera.ScreenPointToRay(MouseManager.GetPixelCoordinatesOfMouse());
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100, IgnoreSnapPointsMask)) {
                if (FloorLayerMask == (FloorLayerMask | (1 << hit.collider.gameObject.layer))) {
                    LastPositions.Clear();
                    CurrentPosRefreshTime = PositionRefreshRate;
                    FinalMoving = false;
                    bDragging = true;
                    panOrigin = GetWorldPosition(0);

                    if (hit.collider.gameObject.CompareTag("Water Volume")) {
                        GameObject spawnedVFX =
                            Instantiate(WaterSplashEffect, hit.point,
                                Quaternion.identity) as GameObject;
                        Destroy(spawnedVFX, 5f);
                        AudioSource audio = spawnedVFX.GetComponent<AudioSource>();
                        audio.volume = 0.1f;
                        audio.PlayOneShot(ClickWaterSound);
                        SubtitlesManager.SetSubtitleText("[splashy]");
                    }
                    else {
                        GameObject spawnedVFX =
                            Instantiate(DustEffect, hit.point + DustEffectSpawnOffset,
                                Quaternion.identity) as GameObject;
                        Destroy(spawnedVFX, 5f);
                        AudioSource audio = spawnedVFX.GetComponent<AudioSource>();
                        audio.volume = 0.07f;
                        audio.PlayOneShot(ClickDustSound);
                        SubtitlesManager.SetSubtitleText("[tapy]");
                    }

                    CursorUIController.SetDragSprite();
                    CursorUIController.SetDragSpriteWorldDuplicate();

                    if (!DraggingDisabled) {
                        WindVFX.gameObject.SetActive(true);
                        WindAudioSource.volume = 0.0f;
                        WindAudioSource.Play();
                        MouseInputController.HoldDisabled = true;
                    }
                }
            }
        }

        if (!bDragging || DraggingDisabled) return;

        if (Input.GetMouseButton(0)) {
            Vector3 direction = panOrigin - GetWorldPosition(groundZ);
            Vector3 rotation = Vector3.RotateTowards(transform.up, direction, 0.05f * Time.deltaTime,
                0.0F);

            float alpha = Mathf.Clamp01(MyMathUtils.Linear(direction.magnitude, 5f, 15f, 0, 1));
            Color color = new Color(WindVFXParticleSystem.startColor.r, WindVFXParticleSystem.startColor.g,
                WindVFXParticleSystem.startColor.b, alpha);
            WindVFXParticleSystem.startColor = color;

            float audioValume = Mathf.Clamp(MyMathUtils.Linear(direction.magnitude, 0f, 20f, 0, 0.1f), 0, 0.1f);
            WindAudioSource.volume = audioValume;

            WindVFX.transform.rotation = Quaternion.LookRotation(rotation);


            Vector3 temp = direction.magnitude * PanSpeed * direction.normalized;
            temp = Vector3.ClampMagnitude(temp, MaxPanSpeed);
            temp *= Time.deltaTime;
            CameraTransform.position += temp;

            CurrentPosRefreshTime += Time.deltaTime;
            if (CurrentPosRefreshTime >= PositionRefreshRate) {
                LastPositions.Add(Camera.ScreenToViewportPoint(MouseManager.GetPixelCoordinatesOfMouse()));
                CurrentPosRefreshTime = 0.0f;
            }
        }

        if (Input.GetMouseButtonUp(0)) {
            ReleaseDrag();
            MouseInputController.HoldDisabled = false;
        }
    }

    void ReleaseDrag() {
        Vector3 pos = Camera.ScreenToViewportPoint(MouseManager.GetPixelCoordinatesOfMouse()) -
                      LastPositions.First();

        Vector3 temp;

        if (pos.magnitude < 0.1) {
            Vector3 direction = panOrigin - GetWorldPosition(groundZ);
            temp = direction.magnitude * PanSpeedBreak * direction.normalized * Time.deltaTime;

            FinalPoint = CameraTransform.position + temp;
            FinalMoving = true;
            bDragging = false;
            return;
        }

        double radians = Mathf.Deg2Rad * CameraTransform.rotation.eulerAngles.y;

        double newX = pos.x * Math.Cos(-radians) - pos.y * Math.Sin(-radians);
        double newY = pos.x * Math.Sin(-radians) + pos.y * Math.Cos(-radians);

        pos = new Vector3((float) newX, (float) newY, 0);

        temp = pos.magnitude * finalMoveStrenght * new Vector3(pos.x, 0, pos.y).normalized;
        FinalPoint = new Vector3(CameraTransform.position.x - temp.x, CameraTransform.position.y,
            CameraTransform.position.z - temp.z);
        FinalMoving = true;
        bDragging = false;
    }

    void MoveToFinalPos() {
        Vector3 vec = FinalPoint - CameraTransform.position;
        Vector3 temp = vec.normalized * finalMoveSpeed * vec.magnitude;
        temp = Vector3.ClampMagnitude(temp, MaxPanSpeed);
        temp *= Time.deltaTime;
        CameraTransform.position += temp;

        float alpha =
            Mathf.Clamp01(MyMathUtils.Linear((FinalPoint - CameraTransform.position).magnitude, 5f, 15f, 0, 1));
        Color color = new Color(WindVFXParticleSystem.startColor.r, WindVFXParticleSystem.startColor.g,
            WindVFXParticleSystem.startColor.b, alpha);
        WindVFXParticleSystem.startColor = color;

        float audioValume =
            Mathf.Clamp(MyMathUtils.Linear((FinalPoint - CameraTransform.position).magnitude, 0f, 20f, 0, 0.1f), 0,
                0.1f);
        WindAudioSource.volume = audioValume;

        if ((FinalPoint - CameraTransform.position).magnitude <= 1) {
            FinalMoving = false;
        }
    }

    public Vector3 GetWorldPosition(float z, bool TipOfHand = false) {
        Ray mousePos;
        if (TipOfHand) {
            mousePos = Camera.ScreenPointToRay(MouseManager.GetPixelCoordinatesOfMouse());
        }
        else {
            mousePos = Camera.ScreenPointToRay(Input.mousePosition);
        }

        Plane ground = new Plane(Vector3.down, new Vector3(0, 0, z));
        float distance;
        ground.Raycast(mousePos, out distance);
        return mousePos.GetPoint(distance);
    }

    void OnDrawGizmos() {
        foreach (var lastPosition in LastPositions) {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(lastPosition, 1f);
        }

        if (LastPositions.Count >= 1) {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(LastPositions.First(), 1f);
        }
    }
}