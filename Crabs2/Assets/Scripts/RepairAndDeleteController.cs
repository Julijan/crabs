﻿using UnityEngine;
using UnityEngine.UI;

public class RepairAndDeleteController : ButtonController {
    public Image Image;
    public Sprite SelectedSprite;
    public Sprite UnSelectedSprite;
    public bool Selected = false;
    public AudioSource AudioSource;
    public AudioClip ClickButtonSound;

    public override void MouseClickedDown() {
        base.MouseClickedDown();

        if(ClickableGameObjectsManager.DoesManagerAllowClicks) {
            Selected = !Selected;
            if (Selected) {
                Image.sprite = SelectedSprite;
            }
            else {
                Image.sprite = UnSelectedSprite;
            }

            AudioSource.pitch = Random.Range(0.8f, 1.3f);
            AudioSource.volume = 0.5f;
            AudioSource.PlayOneShot(ClickButtonSound);
        }
    }

    public void DisableSelection() {
        Image.sprite = UnSelectedSprite;
        Selected = false;
    }
}