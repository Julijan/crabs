﻿using System;
using UnityEngine;

public class OnCollisionExitGame : MonoBehaviour {
    private void OnCollisionEnter(Collision other) {
        Application.Quit();
    }
}