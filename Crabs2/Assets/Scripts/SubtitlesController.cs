﻿using TMPro;
using UnityEngine;

public class SubtitlesController : MonoBehaviour {
    public TextMeshProUGUI SubtitlesText;

    private void Start() {
        SubtitlesManager.Init(SubtitlesText);
    }

    private void Update() {
        SubtitlesManager.Update(Time.deltaTime);
    }
}