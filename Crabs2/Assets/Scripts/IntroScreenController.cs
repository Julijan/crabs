﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class IntroScreenController : MonoBehaviour {
    private int FrameInVideoToLoad = 607;
    public bool StartLoading = false;
    public string SceneToLoad = "";
    public string ThisSceneName = "";
    public VideoPlayer VideoPlayer;

    void Start() {
        VideoPlayer.Play();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
        VideoPlayer.loopPointReached += EndReached;
    }

    private void Update() {
        if (StartLoading) return;
        if (VideoPlayer.frame == FrameInVideoToLoad) {
            StartLoading = true;
            VideoPlayer.Pause();
            StartCoroutine(LoadNewScene());
        }
    }

    void EndReached(UnityEngine.Video.VideoPlayer vp) {
        SceneManager.UnloadSceneAsync(ThisSceneName);
    }

    IEnumerator LoadNewScene() {
        AsyncOperation load = SceneManager.LoadSceneAsync(SceneToLoad, LoadSceneMode.Additive);
        yield return load;
        SceneManager.SetActiveScene( SceneManager.GetSceneByName( SceneToLoad ) );
        VideoPlayer.Play();
    }
}