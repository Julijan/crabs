﻿using UnityEngine;

public class HideRightPanel : MonoBehaviour {
    public RightPanelUIController RightPanelUIController;

    public void HideRightPanelF() {
        RightPanelUIController.DisableUI();
    }
}