﻿using System.Collections.Generic;
using UnityEngine;

public enum RightPanelSelection {
    None,
    Building,
    Furniture,
    Plants
}

public class RightPanelUIController : MonoBehaviour {
    public CursorUIController CursorUIController;

    public Animator RightPanelAnimator;

    private RightPanelSelection RightPanelSelection = RightPanelSelection.None;

    public GameObject BuildingSelectionPanel;
    public GameObject StatsSelectionPanel;
    public GameObject TempSelectionPanel;

    private List<BuilderButtonController> BuilderButtonControllers;
    public List<RepairAndDeleteController> RepairAndDeleteControllers;
    public BuildingController BuildingController;

    public AudioSource AudioSource;
    public AudioClip ClickButtonSound;
    public AudioClip BuilderMenuOpen;
    public AudioClip BuilderMenuClose;

    private void Start() {
        BuilderButtonControllers = new List<BuilderButtonController>();
        BuilderButtonController[] components = Resources.FindObjectsOfTypeAll<BuilderButtonController>();
        foreach (var component in components) {
            BuilderButtonControllers.Add(component);
        }
    }

    public void ClearAllSelection(BuilderButtonController clicked = null,
        RepairAndDeleteController repairAndDelete = null) {
        foreach (var builderButtonController in BuilderButtonControllers) {
            if (builderButtonController != clicked) {
                builderButtonController.DisableSelection();
            }
        }

        foreach (var repairAndDeleteController in RepairAndDeleteControllers) {
            if (repairAndDeleteController != repairAndDelete) {
                repairAndDeleteController.DisableSelection();
            }
        }
    }

    private void PlayClickSound() {
        AudioSource.pitch = Random.Range(0.8f, 1.3f);
        AudioSource.volume = 0.5f;
        AudioSource.PlayOneShot(ClickButtonSound);
    }

    void PlayOpenSound() {
        AudioSource.pitch = 1.0f;
        AudioSource.volume = 0.2f;
        AudioSource.PlayOneShot(BuilderMenuOpen);
    }

    void PlayCloseSound() {
        AudioSource.pitch = 1.0f;
        AudioSource.volume = 0.2f;
        AudioSource.PlayOneShot(BuilderMenuClose);
    }

    public void BuildingSelectionClick() {
        if (RightPanelSelection == RightPanelSelection.None) {
            RightPanelAnimator.SetTrigger("Toggle");
            PlayOpenSound();
        }
        else if (RightPanelSelection == RightPanelSelection.Building) {
            UnselectSelection(true);
            PlayCloseSound();
        }
        else {
            UnselectSelection();
            PlayClickSound();
        }

        BuildingSelectionPanel.SetActive(true);
        StatsSelectionPanel.SetActive(false);
        TempSelectionPanel.SetActive(false);
        RightPanelSelection = RightPanelSelection.Building;
        CursorUIController.ClearSprites();
        BuildingController.ClearToggles();
    }

    public void StatsClick() {
        if (RightPanelSelection == RightPanelSelection.None) {
            RightPanelAnimator.SetTrigger("Toggle");
            PlayOpenSound();
        }
        else if (RightPanelSelection == RightPanelSelection.Furniture) {
            UnselectSelection(true);
            PlayCloseSound();
        }
        else {
            UnselectSelection();
            PlayClickSound();
        }

        BuildingSelectionPanel.SetActive(false);
        StatsSelectionPanel.SetActive(true);
        TempSelectionPanel.SetActive(false);
        RightPanelSelection = RightPanelSelection.Furniture;
        CursorUIController.ClearSprites();
        BuildingController.ClearToggles();
    }

    public void TempClick() {
        if (RightPanelSelection == RightPanelSelection.None) {
            RightPanelAnimator.SetTrigger("Toggle");
            PlayOpenSound();
        }
        else if (RightPanelSelection == RightPanelSelection.Plants) {
            UnselectSelection(true);
            PlayCloseSound();
        }
        else {
            UnselectSelection();
            PlayClickSound();
        }

        BuildingSelectionPanel.SetActive(false);
        StatsSelectionPanel.SetActive(false);
        TempSelectionPanel.SetActive(true);
        RightPanelSelection = RightPanelSelection.Plants;
        CursorUIController.ClearSprites();
        BuildingController.ClearToggles();
    }

    public void RepairBUttonClick(RepairAndDeleteController demo) {
        UnselectSelection(false, demo);
        CursorUIController.SetHandHammerSprite();
    }

    public void DeleteButtonClick(RepairAndDeleteController demo) {
        UnselectSelection(false, demo);
        CursorUIController.SetHandExeSprite();
    }

    public void DisableUI() {
        BuildingSelectionPanel.SetActive(false);
        StatsSelectionPanel.SetActive(false);
        TempSelectionPanel.SetActive(false);
        RightPanelSelection = RightPanelSelection.None;
    }


    public void UnselectSelection(bool clear = false, RepairAndDeleteController demo = null) {
        if (clear) {
            RightPanelSelection = RightPanelSelection.None;
            RightPanelAnimator.SetTrigger("Toggle");
        }

        BuildingController.CleareSelectedBuilding();
        ClearAllSelection(null, demo);
    }
}