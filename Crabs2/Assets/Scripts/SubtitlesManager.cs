﻿using TMPro;

public static class SubtitlesManager {
    private static TextMeshProUGUI SubtitlesText;
    private static float Time = 0.0f;
    private static float SubtitleDuration = 1.5f;

    public static void SetSubtitleText(string text) {
        SubtitlesText.text = text;
        SubtitlesText.gameObject.SetActive(true);
        Time = 0.0f;
    }

    public static void Update(float deltaTime) {
        Time += deltaTime / SubtitleDuration;
        if (Time >= 1) {
            SubtitlesText.gameObject.SetActive(false);
        }
    }

    public static void Init(TextMeshProUGUI text) {
        SubtitlesText = text;
        SubtitlesText.text = "";
    }
}