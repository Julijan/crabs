using System;
using UnityEngine;

public enum TagToSnapTo {
    None,
    BaseSnapPoint,
    WallSnapPoint,
    FreeForm,
}

public class BuildingController : MonoBehaviour {
    public Placeable ObjToMove;
    public Placeable ObjToPlace;
    public LayerMask Mask;
    public LayerMask FloorLayerMask;
    public LayerMask FreeFormMask;
    public Camera Camera;
    public Vector3 VoidPos = new Vector3(-9999, -9999, -9999);

    public Material BaseSnapPointMaterial;
    public Material WallSnapPointMaterial;

    public float SnapPointColorIntensity = 0.3f;

    private TagToSnapTo TagToSnapTo = TagToSnapTo.None;

    private GameObject CurrentlySelectedSnapPoint;


    public float ClickDuration = 0.2f;

    public RightPanelUIController RightPanelUIController;

    bool rotating = false;
    public float speedOfRotation = 0.0f;
    float totalDownTime = 0;

    private float OffsetSpawnOnY = 0.0001f;

    public CursorUIController CursorUIController;

    private float CurrentRotationSnapNumber;
    public float UpperRotationTreshold = 1.0f;
    public float DownerRotationTreshold = -1.0f;

    public AudioSource AudioSource;
    public AudioClip RotationTik;
    public AudioClip DestroyClip;
    public AudioClip EditClip;

    private float TimeTresholdForMiddleMouseClick = 0.2f;
    private float TimeForMiddleMouseClick = -1;

    private bool IsDestruction = false;
    private bool IsEdit = false;
    private bool UnselectAfterPlace = false;


    private void Start() {
        ShowSnapPoints(TagToSnapTo);
    }

    void Update() {
        if (Input.GetMouseButtonUp(1)) {
            if (IsDestruction) {
                Ray ray = Camera.ScreenPointToRay(MouseManager.GetPixelCoordinatesOfMouse());
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, FloorLayerMask)) {
                    if (hit.collider.gameObject.GetComponent<PlaceableLink>()) {
                        if (hit.collider.gameObject.GetComponent<PlaceableLink>().Placeable.CanBePickedUp) {
                            Destroy(hit.collider.gameObject.GetComponent<PlaceableLink>().Placeable.gameObject);

                            AudioSource.PlayOneShot(DestroyClip);
                        }
                    }
                }
            }
            else if (IsEdit) {
                Ray ray = Camera.ScreenPointToRay(MouseManager.GetPixelCoordinatesOfMouse());
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, FloorLayerMask)) {
                    if (hit.collider.gameObject.GetComponent<PlaceableLink>()) {
                        Placeable plac = hit.collider.gameObject.GetComponent<PlaceableLink>().Placeable;
                        if (plac.CanBePickedUp) {
                            if (plac.SnapedOnPoint != null) plac.SnapedOnPoint.SetActive(true);
                            plac.SnapedOnPoint = null;
                            EditSelectedBuilding(plac);
                            Destroy(plac.gameObject);
                            AudioSource.PlayOneShot(EditClip);
                            IsEdit = false;
                            ClickableGameObjectsManager.DoesManagerAllowClicks = false;
                        }
                    }
                }
            }
        }


        if (TagToSnapTo != TagToSnapTo.None) {
            Ray ray = Camera.ScreenPointToRay(MouseManager.GetPixelCoordinatesOfMouse());
            RaycastHit hit;
            if (ObjToPlace.TagToSnapTo == TagToSnapTo.FreeForm) {
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, FreeFormMask)) {
                    ObjToMove.transform.position = new Vector3(hit.point.x, hit.point.y + OffsetSpawnOnY, hit.point.z);
                }
            }
            else if (Physics.Raycast(ray, out hit, Mathf.Infinity, Mask)) {
                if (hit.collider.gameObject.CompareTag(ObjToPlace.TagToSnapTo.ToString())) {
                    CurrentlySelectedSnapPoint = hit.collider.gameObject;
                    float y = hit.collider.gameObject.transform.position.y + (ObjToMove.transform.localScale.y / 2f)
                        ;
                    Vector3 pos = new Vector3(hit.collider.gameObject.transform.position.x, y,
                        hit.collider.gameObject.transform.position.z);
                    ObjToMove.transform.position = pos;
                    if (ObjToPlace.TagToSnapTo == TagToSnapTo.WallSnapPoint) {
                        ObjToMove.transform.rotation = hit.collider.gameObject.transform.rotation;
                    }
                }
            }


            if (ObjToMove.transform.position != VoidPos && Input.GetMouseButtonDown(1)) {
                totalDownTime = 0;
            }

            if (Input.GetMouseButtonDown(2)) {
                if (ObjToMove.CanRotate) {
                    TimeForMiddleMouseClick = Time.time;
                }
            }

            if (Input.GetMouseButtonUp(2)) {
                if (TimeForMiddleMouseClick != -1) {
                    if ((Time.time - TimeForMiddleMouseClick) < TimeTresholdForMiddleMouseClick) {
                        TimeForMiddleMouseClick = -1;
                        RotateNinghtyDegrees();
                    }
                }
            }

            if (Input.GetMouseButton(1)) {
                totalDownTime += Time.deltaTime;

                if (totalDownTime >= ClickDuration) {
                    CursorUIController.SetRotateSprite();
                    RotateObject();
                    rotating = true;
                }
            }

            if (Input.GetMouseButtonUp(1)) {
                if (!rotating) {
                    TryPlaceObject();
                }
                else {
                    CursorUIController.SetIdleSprite();
                }

                rotating = false;
            }
        }
    }

    public void ClearToggles() {
        IsDestruction = false;
        IsEdit = false;
    }

    private void RotateNinghtyDegrees() {
        Vector3 alignedForward = NearestWorldAxis(ObjToMove.transform.forward);
        Vector3 alignedUp = NearestWorldAxis(ObjToMove.transform.up);

        ObjToMove.transform.rotation = Quaternion.LookRotation(alignedForward, alignedUp);

        ObjToMove.transform.rotation *= Quaternion.Euler(0, 90, 0);
    }

    private static Vector3 NearestWorldAxis(Vector3 v) {
        if (Mathf.Abs(v.x) < Mathf.Abs(v.y)) {
            v.x = 0;
            if (Mathf.Abs(v.y) < Mathf.Abs(v.z))
                v.y = 0;
            else
                v.z = 0;
        }
        else {
            v.y = 0;
            if (Mathf.Abs(v.x) < Mathf.Abs(v.z))
                v.x = 0;
            else
                v.z = 0;
        }

        return v;
    }

    void TryPlaceObject() {
        if (ObjToMove.CanPlace) {
            Placeable temp = Instantiate(ObjToPlace, ObjToMove.transform.position,
                ObjToMove.transform.rotation);
            temp.SpawnIt(ObjToPlace);
            ObjToMove.transform.position = VoidPos;
            if (CurrentlySelectedSnapPoint != null) {
                temp.SnapedOnPoint = CurrentlySelectedSnapPoint;
                CurrentlySelectedSnapPoint.SetActive(false);
            }

            if (UnselectAfterPlace) {
                UnselectAfterPlace = false;
                CleareSelectedBuilding();
                RightPanelUIController.UnselectSelection(false);
                CursorUIController.ClearSprites();
                ClickableGameObjectsManager.DoesManagerAllowClicks = true;
            }
        }
    }

    void RotateObject() {
        if (TagToSnapTo == TagToSnapTo.FreeForm) {
            ObjToMove.transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X"), 0) * Time.deltaTime *
                                       speedOfRotation);
        }
        else if (ObjToMove.CanRotate) {
//            CurrentRotationSnapNumber += Input.GetAxis("Mouse X");
//            Debug.Log(CurrentRotationSnapNumber);
//            if (CurrentRotationSnapNumber > UpperRotationTreshold) {
//                AudioSource.PlayOneShot(RotationTik);
//                CurrentRotationSnapNumber = 0.0f;
//                ObjToMove.transform.rotation *= Quaternion.Euler(0, 90, 0);
//            }
//            else if (CurrentRotationSnapNumber < DownerRotationTreshold) {
//                AudioSource.PlayOneShot(RotationTik);
//                CurrentRotationSnapNumber = 0.0f;
//                ObjToMove.transform.rotation *= Quaternion.Euler(0, 90, 0);
//            }
        }
    }

    public void ChangeSelectedBuilding(Placeable placeable) {
        IsDestruction = false;
        IsEdit = false;

        if (ObjToMove != null) {
            Destroy(ObjToMove.gameObject);
        }

        ObjToMove = Instantiate(placeable, VoidPos, Quaternion.identity);
        ObjToMove.GetComponent<Placeable>().GhostMode();
        ObjToPlace = placeable;
        ShowSnapPoints(ObjToPlace.TagToSnapTo);
        CursorUIController.SetHandHandSprite(true);
    }

    public void EditSelectedBuilding(Placeable placeable) {
        IsDestruction = false;
        IsEdit = false;

        ObjToMove = Instantiate(placeable.Prefab, VoidPos, Quaternion.identity);
        ObjToMove.GetComponent<Placeable>().GhostMode();
        ObjToPlace = placeable.Prefab;
        ShowSnapPoints(ObjToPlace.TagToSnapTo);
        CursorUIController.SetHandHandSprite(true);
        UnselectAfterPlace = true;
    }

    public void ToggleDestruction() {
        IsDestruction = !IsDestruction;
        IsEdit = false;
    }

    public void ToggleRepair() {
        IsEdit = !IsEdit;
        IsDestruction = false;
    }

    public void CleareSelectedBuilding() {
        if (ObjToMove != null) {
            Destroy(ObjToMove.gameObject);
        }

        ObjToMove = null;
        ObjToPlace = null;
        TagToSnapTo = TagToSnapTo.None;
        ShowSnapPoints(TagToSnapTo);
        CursorUIController.SetHandHandSprite();
    }

    void ShowSnapPoints(TagToSnapTo tagToSnapTo) {
        TagToSnapTo = tagToSnapTo;

//        BaseSnapPointMaterial.color = new Color(BaseSnapPointMaterial.color.r, BaseSnapPointMaterial.color.g,
//            BaseSnapPointMaterial.color.b, 0f);
//        WallSnapPointMaterial.color = new Color(WallSnapPointMaterial.color.r, WallSnapPointMaterial.color.g,
//            WallSnapPointMaterial.color.b, 0f);
//
//
//        switch (TagToSnapTo) {
//            case TagToSnapTo.BaseSnapPoint: {
//                BaseSnapPointMaterial.color = new Color(BaseSnapPointMaterial.color.r, BaseSnapPointMaterial.color.g,
//                    BaseSnapPointMaterial.color.b, SnapPointColorIntensity);
//                break;
//            }
//            case TagToSnapTo.WallSnapPoint: {
//                WallSnapPointMaterial.color = new Color(WallSnapPointMaterial.color.r, WallSnapPointMaterial.color.g,
//                    WallSnapPointMaterial.color.b, SnapPointColorIntensity);
//                break;
//            }
//        }
    }
}