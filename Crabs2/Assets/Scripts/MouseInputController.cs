﻿using UnityEngine;

public class MouseInputController : MonoBehaviour {
    public Camera Camera;

    public ClickableGameObject CurrentlyClickedGameObject;

    public bool ClickDownDisabled = false;
    public bool ClickUpDisabled = false;
    public bool HoldDisabled = false;

    void Update() {
        if (Input.GetMouseButtonDown(0) && !ClickDownDisabled) {
            Ray ray = Camera.ScreenPointToRay(MouseManager.GetPixelCoordinatesOfMouse());
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100)) {
                CurrentlyClickedGameObject = hit.collider.GetComponent<ClickableGameObject>();
                if (CurrentlyClickedGameObject != null && CurrentlyClickedGameObject.Interectable) {
                    CurrentlyClickedGameObject.MouseClickedDown();
                }
            }
        }

        if (Input.GetMouseButtonUp(0) && !ClickUpDisabled) {
            Ray ray = Camera.ScreenPointToRay(MouseManager.GetPixelCoordinatesOfMouse());
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100)) {
                CurrentlyClickedGameObject = hit.collider.GetComponent<ClickableGameObject>();
                if (CurrentlyClickedGameObject != null && CurrentlyClickedGameObject.Interectable) {
                    CurrentlyClickedGameObject.MouseClickedUp();
                }
            }
        }

        if (Input.GetMouseButton(0) && !HoldDisabled) {
            Ray ray = Camera.ScreenPointToRay(MouseManager.GetPixelCoordinatesOfMouse());
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100)) {
                ClickableGameObject temp = hit.collider.GetComponent<ClickableGameObject>();
                if (CurrentlyClickedGameObject != temp) {
                    if (CurrentlyClickedGameObject != null && CurrentlyClickedGameObject.Interectable) {
                        CurrentlyClickedGameObject.MouseClickedFocusLost();
                    }

                    if (temp != null && temp.Interectable) {
                        temp.MouseClickedFocusEnter();
                    }

                    CurrentlyClickedGameObject = temp;
                }
            }
        }
    }
}