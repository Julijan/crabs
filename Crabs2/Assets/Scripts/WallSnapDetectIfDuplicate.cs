﻿using UnityEngine;

public class WallSnapDetectIfDuplicate : MonoBehaviour {
    public bool DontCheck;
    public Collider ColliderMe;

    private void Start() {
        if (DontCheck) return;
        Collider[] colliders = Physics.OverlapSphere(transform.position, 0.5f);
        if (colliders.Length > 0) {
            foreach (var collider1 in colliders) {
                if (collider1 != ColliderMe && this.gameObject.CompareTag(collider1.gameObject.tag)) {
                    collider1.gameObject.GetComponent<WallSnapDetectIfDuplicate>().DontCheck = true;
                    this.gameObject.SetActive(false);
                }
            }
        }
    }
}