﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class LoadingScreenController : MonoBehaviour {
    public string ThisSceneName = "";
    public VideoPlayer VideoPlayer;
    public int FrameInVideoToLoad = 105;
    public bool StartLoading = false;

    void Start() {
        VideoPlayer.Play();

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;

        VideoPlayer.loopPointReached += EndReached;
    }

    void EndReached(UnityEngine.Video.VideoPlayer vp) {
        SceneManager.UnloadSceneAsync(ThisSceneName);
    }

    private void Update() {
        if (StartLoading) return;
        if (VideoPlayer.frame == FrameInVideoToLoad) {
            StartLoading = true;
            VideoPlayer.Pause();
            StartCoroutine(UnloadAndLoadScene());
        }
    }

    IEnumerator UnloadAndLoadScene() {
        AsyncOperation load = SceneManager.UnloadSceneAsync(SceneLoaderManager.ScreenToUnLoad);
        yield return load;
        load = SceneManager.LoadSceneAsync(SceneLoaderManager.ScreenToLoad, LoadSceneMode.Additive);
        yield return load;
        SceneManager.SetActiveScene( SceneManager.GetSceneByName( SceneLoaderManager.ScreenToLoad ) );
        VideoPlayer.Play();
    }
}