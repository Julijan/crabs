﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CursorUIController : MonoBehaviour {
    [Header("CurrentCursor")] public RectTransform RectTransformImage;
    public Image Image;
    public Transform MousePointerPos;

    [Header("Left")] public RectTransform RectTransformImageLeft;
    public Image ImageLeft;
    public Transform MousePointerPosLeft;

    [Header("RightCursor")] public RectTransform RectTransformImageRight;
    public Image ImageRight;
    public Transform MousePointerPosRight;
    public LayerMask RightCursorLayerMask;

    [Header("Main stuff")] public Camera Camera;
    public RectTransform RectTransformImageDuplicate;
    public Sprite IdleCoursor;
    public Sprite ClickedCoursor;
    public Sprite DragSprite;
    public Sprite RotateSprite;
    public Sprite RightPanelClickSprite;
    public Sprite HandHammerSprite;
    public Sprite HandExeSprite;
    public Sprite HandHandSprite;

    public LayerMask FloorLayerMask;


    public CameraDragController CameraDragController;

    public RectTransform canvasRectT;
    private Vector2 uiOffset;
    public Vector2 duplicateOffset;

    public LayerMask IgnoreSnapPointsMask;

    private bool Dragging;

    private bool clampedToLeft;
    private bool clampedToRight;
    private bool clampedToTop;
    private bool clampedToBottom;

    private bool IsBuildCursor = false;
    private bool IsExCursor = false;
    private bool IsHandCursor = false;
    private bool IsRightCursor = false;

    public Vector2 ReferenceScaleCursor;

    public bool ClickedUIThisFrame = false;

    public bool IsEnabled = true;

    private void Start() {
        ReferenceScaleCursor = RectTransformImage.sizeDelta;
        Image.material = Instantiate(Image.material);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
        MouseManager.MousePointerPosition = MousePointerPos;
        MouseManager.Camera = Camera;
        MouseManager.CursorUIController = this;
        RectTransformImage.anchoredPosition = Vector2.zero;
        // Calculate the screen offset
        uiOffset = new Vector2((float) canvasRectT.sizeDelta.x / 2f, (float) canvasRectT.sizeDelta.y / 2f);
    }

    private void Update() {
        if (!IsEnabled) return;
#if UNITY_EDITOR
        if (Input.GetKeyDown("space")) {
            RectTransformImage.anchoredPosition = Vector2.zero;
        }
#endif

        CheckIfRightTriggerHitAndChangeSprite();

        if (Input.GetMouseButtonDown(0) && !ClickedUIThisFrame) {
            Ray ray = Camera.ScreenPointToRay(MouseManager.GetPixelCoordinatesOfMouse());
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100, IgnoreSnapPointsMask)) {
                if (FloorLayerMask == (FloorLayerMask | (1 << hit.collider.gameObject.layer))) {
                    Dragging = true;
                    SetDragSprite();
                }
                else {
                    SetClickedSprite();
                }
            }
        }

        if (Dragging) {
            SetDragSpriteWorldDuplicate();
        }

        if (Input.GetMouseButtonUp(0)) {
            var tempColor = Image.color;
            tempColor.a = 1f;
            Image.color = tempColor;
            Dragging = false;
            SetIdleSprite();
            RectTransformImageDuplicate.gameObject.SetActive(false);
            RectTransformImage.gameObject.SetActive(true);
        }


        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRectT, Input.mousePosition, Camera, out pos);
        Vector3 temp = canvasRectT.TransformPoint(pos);
//        RectTransformImage.transform.position = temp;
        RectTransformImageRight.transform.position = temp;
        RectTransformImageLeft.transform.position = temp;


        ClampRect(RectTransformImage);
        ClampRect(RectTransformImageRight);

        float scale =
            MyMathUtils.Linear(
                Vector3.Distance(CameraDragController.GetWorldPosition(0, true), Camera.transform.position),
                9, 41, 1, 0.3f);
        scale = Mathf.Clamp(scale, 0.3f, 1f);

        RectTransformImageLeft.sizeDelta = new Vector2(ReferenceScaleCursor.x * scale, ReferenceScaleCursor.y * scale);
        RectTransformImageRight.sizeDelta = new Vector2(ReferenceScaleCursor.x * scale, ReferenceScaleCursor.y * scale);
    }

    void CheckIfRightTriggerHitAndChangeSprite() {
        ClickedUIThisFrame = false;
        PointerEventData cursor = new PointerEventData(EventSystem.current);
        cursor.position = Camera.WorldToScreenPoint(MousePointerPosRight.position);
        List<RaycastResult> objectsHit = new List<RaycastResult>();
        EventSystem.current.RaycastAll(cursor, objectsHit);

        bool rightCursor = false;
        foreach (var raycastResult in objectsHit) {
            if (Input.GetMouseButtonDown(0)) {
                ClickableGameObject currentlyClickedGameObject =
                    raycastResult.gameObject.GetComponent<ClickableGameObject>();
                if (currentlyClickedGameObject != null && currentlyClickedGameObject.Interectable) {
                    currentlyClickedGameObject.MouseClickedDown();
                    ClickedUIThisFrame = true;
                }
            }

            if (raycastResult.gameObject.CompareTag("RightCursor")) {
                rightCursor = true;
                break;
            }
        }

        if (rightCursor) {
            ChangeSpritIfHitRightPanelButtonButton(true);
        }
        else {
            ChangeSpritIfHitRightPanelButtonButton(false);
        }
    }

    public void ChangeSpritIfHitRightPanelButtonButton(bool enable) {
        if (enable) {
            IsRightCursor = true;
            RectTransformImageRight.gameObject.SetActive(true);
            RectTransformImageLeft.gameObject.SetActive(false);
            RectTransformImage = RectTransformImageRight;
            Image = ImageRight;
            MousePointerPos = MousePointerPosRight;
            MouseManager.MousePointerPosition = MousePointerPos;
        }
        else {
            IsRightCursor = false;
            RectTransformImageLeft.gameObject.SetActive(true);
            RectTransformImageRight.gameObject.SetActive(false);
            RectTransformImage = RectTransformImageLeft;
            Image = ImageLeft;
            MousePointerPos = MousePointerPosLeft;
            MouseManager.MousePointerPosition = MousePointerPos;
        }
    }

    // Clamp panel to area of parent
    void ClampRect(RectTransform rectTransform) {
        Vector3 pos = rectTransform.localPosition;

        Vector3 minPosition = canvasRectT.rect.min - rectTransform.rect.min;
        Vector3 maxPosition = canvasRectT.rect.max - rectTransform.rect.max;

        pos.x = Mathf.Clamp(rectTransform.localPosition.x, minPosition.x, maxPosition.x);
        pos.y = Mathf.Clamp(rectTransform.localPosition.y, minPosition.y, maxPosition.y);

        rectTransform.localPosition = pos;
    }

    void ClampToWindow(RectTransform rectTransform) {
        Vector3[] canvasCorners = new Vector3[4];
        Vector3[] panelRectCorners = new Vector3[4];
        canvasRectT.GetWorldCorners(canvasCorners);
//        Debug.Log("canvasCorners" + canvasCorners[0] + " " + canvasCorners[1] + " " + canvasCorners[2] + " " +
//                  canvasCorners[3] + " ");
        rectTransform.GetWorldCorners(panelRectCorners);
//        Debug.Log("panelRectCorners" + panelRectCorners[0] + " " + panelRectCorners[1] + " " + panelRectCorners[2] +
//                  " " + panelRectCorners[3] + " ");

        Debug.Log("NIC " + canvasCorners[0].x + " ENA " + canvasCorners[1].x + " DVA " + canvasCorners[2].x +
                  " TRI " +
                  canvasCorners[3].x);

//        Debug.Log("canvasCorners" + canvasCorners[2].x);
        if (panelRectCorners[2].x > canvasCorners[2].x) {
            if (!clampedToRight) {
                clampedToRight = true;
            }
        }
        else if (clampedToRight) {
            clampedToRight = false;
        }
        else if (panelRectCorners[0].x < canvasCorners[0].x) {
            if (!clampedToLeft) {
                clampedToLeft = true;
            }
        }
        else if (clampedToLeft) {
            clampedToLeft = false;
        }

        if (panelRectCorners[2].y > canvasCorners[2].y) {
            if (!clampedToTop) {
                clampedToTop = true;
            }
        }
        else if (clampedToTop) {
            clampedToTop = false;
        }
        else if (panelRectCorners[0].y < canvasCorners[0].y) {
            if (!clampedToBottom) {
                clampedToBottom = true;
            }
        }
        else if (clampedToBottom) {
            clampedToBottom = false;
        }
    }

    public void SetClickedSprite() {
        if (IsRightCursor || IsBuildCursor || IsExCursor || IsHandCursor) return;
        Image.sprite = ClickedCoursor;
    }

    public void SetIdleSprite() {
        if (IsRightCursor) return;
        if (IsBuildCursor) {
            Image.sprite = HandHammerSprite;
        }
        else if (IsExCursor) {
            Image.sprite = HandExeSprite;
        }
        else if (IsHandCursor) {
            Image.sprite = HandHandSprite;
        }
        else {
            Image.sprite = IdleCoursor;
        }
    }

    public void SetDragSprite() {
        if (IsRightCursor || IsBuildCursor || IsExCursor || IsHandCursor) return;
        Image.sprite = DragSprite;
    }

    public void SetRotateSprite() {
//        if (IsRightCursor || IsBuildOrDestroyCursor) return;
        if (Image != ImageRight) {
            Image.sprite = RotateSprite;
        }
    }

    public void SetHandHammerSprite() {
//        if (IsRightCursor) return;
//        ImageRight.sprite = HandHammerSprite;

        IsBuildCursor = !IsBuildCursor;
        if (IsBuildCursor) {
            ImageLeft.sprite = HandHammerSprite;
        }
        else {
            ImageLeft.sprite = IdleCoursor;
        }

        IsExCursor = false;
        IsHandCursor = false;
    }

    public void SetHandExeSprite() {
//        if (IsRightCursor) return;
//        ImageRight.sprite = HandExeSprite;

        IsExCursor = !IsExCursor;
        if (IsExCursor) {
            ImageLeft.sprite = HandExeSprite;
        }
        else {
            ImageLeft.sprite = IdleCoursor;
        }

        IsBuildCursor = false;
        IsHandCursor = false;
    }

    public void SetHandHandSprite(bool forceTrue = false) {
        IsHandCursor = !IsHandCursor || forceTrue;
        if (IsHandCursor) {
            ImageLeft.sprite = HandHandSprite;
        }
        else {
            ImageLeft.sprite = IdleCoursor;
        }

        IsBuildCursor = false;
        IsExCursor = false;
    }

    public void ClearSprites() {
        ImageLeft.sprite = IdleCoursor;
        IsHandCursor = false;
        IsBuildCursor = false;
        IsExCursor = false;
    }

    public void SetDragSpriteWorldDuplicate() {
        var tempColor = Image.color;
        tempColor.a = 0.3f;
        Image.color = tempColor;

        Vector2 ViewportPosition = Camera.WorldToViewportPoint(CameraDragController.panOrigin);
        Vector2 proportionalPosition =
            new Vector2(ViewportPosition.x * canvasRectT.sizeDelta.x, ViewportPosition.y * canvasRectT.sizeDelta.y);


        float scale =
            MyMathUtils.Linear(Vector3.Distance(CameraDragController.panOrigin, Camera.transform.position),
                9, 41, 1, 0.3f);
        scale = Mathf.Clamp(scale, 0.3f, 1f);

        RectTransformImageDuplicate.sizeDelta =
            new Vector2(ReferenceScaleCursor.x * scale, ReferenceScaleCursor.y * scale);

        // Set the position and remove the screen offset
        RectTransformImageDuplicate.localPosition =
            (proportionalPosition - uiOffset);

        ClampRect(RectTransformImageDuplicate);

        RectTransformImageDuplicate.gameObject.SetActive(true);
        RectTransformImage.gameObject.SetActive(true);
    }
}