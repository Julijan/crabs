﻿using System;
using UnityEngine;

public class CameraRotatingController : MonoBehaviour {
    public CursorUIController CursorUIController;
    public GameObject Camera;
    public GameObject CameraForRotationUp;
    public float SpeedOfRotation;

    private void Update() {
        if (Input.GetMouseButton(2)) {
            Camera.transform.Rotate(Vector3.up, SpeedOfRotation * Time.deltaTime * Input.GetAxis("Mouse X"),
                Space.World);
            CameraForRotationUp.transform.Rotate(Vector3.left,
                SpeedOfRotation * Time.deltaTime * Input.GetAxis("Mouse Y"),
                Space.Self);
            CursorUIController.SetRotateSprite();
        }
        else if (Input.GetMouseButtonUp(2)) {
            CursorUIController.SetIdleSprite();
        }
    }
}