﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class SrcekController : ClickableGameObject {
    public AudioSource AudioSource;
    public AudioClip PopSoundClip;
    public AudioClip BubbleSoundClip;
    public AudioClip ClickSoundClip;
    public AudioClip WaterSplashInSoundClip;
    public Animator Animator;
    public Rigidbody Rigidbody;
    public MeshCollider MeshCollider;
    public GameObject DustEffect;
    public GameObject WaterSplash;
    public MeshRenderer MeshRenderer;

    private void OnCollisionEnter(Collision other) {
        if (other.gameObject.CompareTag("Floor")) {
            if (!AudioSource.isPlaying && other.relativeVelocity.magnitude >= 5) {
                AudioSource.pitch = Random.Range(0f, 2f);
                AudioSource.volume = other.relativeVelocity.magnitude / 1000;
                AudioSource.PlayOneShot(PopSoundClip);
                GameObject spawnedVFX =
                    Instantiate(DustEffect, other.GetContact(0).point,
                        Quaternion.identity) as GameObject;
                Destroy(spawnedVFX, 5f);
            }
        }
        else if (other.gameObject.CompareTag("Destroy")) {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Water Volume")) {
            if (!AudioSource.isPlaying) {
                AudioSource.pitch = Random.Range(0f, 2f);
                AudioSource.volume = Rigidbody.velocity.magnitude / 200;
                AudioSource.PlayOneShot(WaterSplashInSoundClip);


                GameObject spawnedVFX =
                    Instantiate(WaterSplash,
                        other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position),
                        Quaternion.identity) as GameObject;
                Destroy(spawnedVFX, 5f);
            }
        }
    }

    public IEnumerator Destroy(float delayTime) {
        yield return new WaitForSeconds(Random.Range(0f, delayTime));
        Animator.SetTrigger("Die");
    }

    public void OnDestroyAnimationEnd() {
        Destroy(gameObject);
    }

    public void PlayDieAudio() {
        AudioSource.pitch = Random.Range(0.5f, 1.5f);
        AudioSource.volume = 0.1f;
        AudioSource.PlayOneShot(BubbleSoundClip);
    }

    public override void MouseClickedDown() {
        ClickedOn();
    }

    public override void MouseClickedFocusEnter() {
        ClickedOn();
    }

    void ClickedOn() {
        Animator.SetTrigger("Clicked");
        AudioSource.pitch = Random.Range(0.6f, 1.2f);
        AudioSource.volume = 0.02f;
        AudioSource.PlayOneShot(ClickSoundClip);
        Rigidbody.AddExplosionForce(7000, transform.position, 10f);
        SubtitlesManager.SetSubtitleText("[squeezes]");
    }

    public void DisableColliders() {
        MeshCollider.enabled = false;
    }
}