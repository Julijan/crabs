﻿using UnityEngine;

public class BoxGizmoDrawer : MonoBehaviour {
    public Vector3 Size;
    public Color Color;

    void OnDrawGizmos() {
        Gizmos.color = Color;
        Gizmos.DrawCube(transform.position, Size);
    }
}