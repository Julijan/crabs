﻿using UnityEngine;

public class SphereGizmoDrawer : MonoBehaviour {
    public float Radious;
    public Color Color;

    void OnDrawGizmos() {
        Gizmos.color = Color;
        Gizmos.DrawSphere(transform.position, Radious);
    }
}