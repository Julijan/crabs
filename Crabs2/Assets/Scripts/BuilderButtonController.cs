﻿using UnityEngine;

public class BuilderButtonController : ClickableGameObject {
    public GameObject ClickSprite;
    public Placeable Building;
    public BuildingController BuildingController;
    public RightPanelUIController RightPanelUIController;
    public AudioSource AudioSource;
    public AudioClip ClickButtonSound;

    public override void MouseClickedDown() {
        if (ClickableGameObjectsManager.DoesManagerAllowClicks) {
            //order is important for UI 

            if (ClickSprite.activeSelf) {
                BuildingController.CleareSelectedBuilding();
            }
            else {
                BuildingController.ChangeSelectedBuilding(Building);
                RightPanelUIController.ClearAllSelection(this);
            }

            ClickSprite.SetActive(!ClickSprite.activeSelf);
            AudioSource.pitch = Random.Range(0.8f, 1.3f);
            AudioSource.volume = 0.5f;
            AudioSource.PlayOneShot(ClickButtonSound);
        }
    }

    public void DisableSelection() {
        ClickSprite.SetActive(false);
    }
}