﻿using System;
using UnityEngine;

public class GridController : MonoBehaviour {
    public int Rows;
    public int Collums;

    public float Padding;

    public GameObject Cube;

    public Transform StartOfGrid;

    public GameObject[,] Grid;

    public GameObject Cointeiner;

    private void Start() {
        Grid = new GameObject[Rows, Collums];
        for (int i = 0; i < Rows; i++) {
            for (int j = 0; j < Collums; j++) {
                Vector3 pos = new Vector3(StartOfGrid.position.x + i * Padding, StartOfGrid.position.y,
                    StartOfGrid.position.z + j * Padding);
                Grid[i, j] = Instantiate(Cube, pos, Quaternion.identity, Cointeiner.transform);
            }
        }
    }

//    private void Update() {
//        for (int i = 0; i < Rows; i++) {
//            for (int j = 0; j < Collums; j++) {
//                Vector3 pos = new Vector3(StartOfGrid.position.x + i * Padding, StartOfGrid.position.y,
//                    StartOfGrid.position.z + j * Padding);
//                Grid[i, j].transform.position = pos;
//            }
//        }
//    }
}