﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CollisionCache : MonoBehaviour {
    public List<GameObject> GameObjectsToching = new List<GameObject>();

    public bool IgnoreBaseBuildingCollisions;

    public bool IsTocuhingSomething {
        get { return GameObjectsToching.Count > 0; }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.isTrigger) return;
        if (IgnoreBaseBuildingCollisions) {
            if (!other.gameObject.CompareTag("BaseBuilding")) {
            
                GameObjectsToching.Add(other.gameObject);
            }
        }
        else {
            GameObjectsToching.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.isTrigger) return;
        if (IgnoreBaseBuildingCollisions) {
            if (!other.gameObject.CompareTag("BaseBuilding")) {
                GameObjectsToching.Remove(other.gameObject);
            }
        }
        else {
            GameObjectsToching.Remove(other.gameObject);
        }
    }
}